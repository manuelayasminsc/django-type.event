# Type.Event
>Status: Finalizado

## Sobre
Esse projeto foi feito usando a linguagem de programação Python e o framework Django. Se trata de um site para empresas registrarem seus eventos educacionais e consequentemente para os participantes terem acesso as informações de um determinado evento.

O projeto foi realizado durante a PSW 6.0 para fins de estudos.

## Funcionalidades

**Área para empresas:**

-   Cadastro e login de usuários.
-   Página para criar um novo evento.
-   Página para gerenciar os eventos criados.
-   Gerar certificados.
-   Página para administrar certificados dos participantes de um evento.
-   Busca de certificados.

**Área para participantes:**

-   Cadastro e login de usuários.
-   Página para inscrição de um evento.
-   Página para acessar certificados.
-   Página para acessar os eventos que o participante se inscreveu.

## Requisitos

**Rode esse comando para que as dependências necessárias sejam instaladas:**

`$ pip install -r requirements.txt`

## Executando em ambiente de desenvolvimento

1.  `python3 manage.py migrate`
2.  `python3 manage.py runserver`
3.  Abra o navegador e explore o projeto.

##  Leitura Complementar

**Documentação oficial do Django:**
https://docs.djangoproject.com/en/4.2/

